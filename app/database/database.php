<?php
namespace App\Database;

use \App\Config\Config;
use \App\Helpers\Util;
use \PDO;

class Database {
	var $config = null;
	var $util = null;

	public function __construct() {
		# Get the app config data
		$this->config = Config::slim_config();

		# Create the utility object
		$this->util = new Util();
	}

	/**
	 * Initialize the database, complete with sample data.
	 */
	public function init() {
		# Define the steps needed to initialize the database
		$steps = array(
			'create_db',
			'create_order',
			'create_order_items',
			'create_category',
			'create_product'
		);

		$total_steps = 0;
		$total_steps += 1;        // Create database
		$total_steps += 1;        // Create Order table
		$total_steps += 1;        // Create Order Items table
		$total_steps += 1 + 500;  // Create Category table and sample data
		$total_steps += 1 + 5000; // Create Product table and sample data

		# Initialize the session
		session_start();

		$_SESSION['processed'] = 0;
		$_SESSION['total'] = $total_steps;
		$_SESSION['progress'] = 0;
		$_SESSION['description'] = 'Starting the database migration';

		# Write the data
		session_write_close();

		# Start the migration
		foreach($steps as $i => $function) {
			$result = $this->$function();

			if($result === false) {
				return;
			}
		}

		$this->set_migration_progress($total_steps, 'Database migration successful - refreshing page');
	}

	/**
	 * Get the current migration progress.
	 */
	public function migration_progress() {
		session_start();

		return array(
			'processed'   => $_SESSION['processed'],
			'total'       => $_SESSION['total'],
			'progress'    => $_SESSION['progress'],
			'description' => $_SESSION['description'] . '...'
		);
	}

	/**
	 * Update the session that tracks the migration progress.
	 */
	private function set_migration_progress($processed, $description) {
		session_start();

		$_SESSION['processed']   = $processed;
		$_SESSION['progress']    = ($processed / $_SESSION['total']) * 100;
		$_SESSION['description'] = $description;

		session_write_close();
	}

	/**
	 * Create the database.
	 */
	private function create_db() {
		session_start();
		$this->set_migration_progress($_SESSION['processed'] + 1, 'Creating the ' . ucfirst($this->config['db']['dbname']) . ' database');

		try {
			$pdo = new PDO('mysql:host=' . $this->config['db']['host'], $this->config['db']['user'], $this->config['db']['pass']);

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

			$pdo->exec("CREATE DATABASE `{$this->config['db']['dbname']}`;
				GRANT ALL ON `{$this->config['db']['dbname']}`.* TO '{$this->config['db']['user']}'@'localhost';
				FLUSH PRIVILEGES;");
		} catch(PDOException $e) {
			return false;
		}

		return true;
	}

	/**
	 * Create the Order table.
	 */
	private function create_order() {
		session_start();
		$this->set_migration_progress($_SESSION['processed'] + 1, 'Creating the Order table');

		try {
			$pdo = new PDO('mysql:host=' . $this->config['db']['host'] . ';dbname=' . $this->config['db']['dbname'], $this->config['db']['user'], $this->config['db']['pass']);

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

			$pdo->exec("CREATE table `orders` (
					`id` INT(11) AUTO_INCREMENT PRIMARY KEY,
					`order_date` DATETIME NOT NULL
				);");
		} catch(PDOException $e) {
			return false;
		}

		return true;
	}

	private function create_order_items() {
		session_start();
		$this->set_migration_progress($_SESSION['processed'] + 1, 'Creating the Order Items table');

		try {
			$pdo = new PDO('mysql:host=' . $this->config['db']['host'] . ';dbname=' . $this->config['db']['dbname'], $this->config['db']['user'], $this->config['db']['pass']);

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

			$pdo->exec("CREATE table `order_items` (
					`id` INT(11) AUTO_INCREMENT PRIMARY KEY,
					`order_id` INT(11) NOT NULL,
					`product_id` INT(11) NOT NULL,
					`quantity` INT(11) NOT NULL
				);");
		} catch(PDOException $e) {
			return false;
		}

		return true;
	}

	/**
	 * Create the Category table.
	 */
	private function create_category() {
		session_start();
		$this->set_migration_progress($_SESSION['processed'] + 1, 'Creating the Category table');

		try {
			$pdo = new PDO('mysql:host=' . $this->config['db']['host'] . ';dbname=' . $this->config['db']['dbname'], $this->config['db']['user'], $this->config['db']['pass']);

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

			# Create the table
			$pdo->exec("CREATE table `categories` (
					`id` INT(11) AUTO_INCREMENT PRIMARY KEY,
					`label` VARCHAR(128) NOT NULL
				);");

			# Add sample data
			$sql = 'INSERT INTO categories(label) VALUES(:label)';
			$stmt = $pdo->prepare($sql);

			for($i = 1; $i <= 500; $i++) {
				$this->set_migration_progress($_SESSION['processed'] + 1, 'Adding data for Category ' . str_pad($i, 3, '0', STR_PAD_LEFT));
				$stmt->execute([':label' => 'Category ' . str_pad($i, 3, '0', STR_PAD_LEFT)]);
			}
		} catch(PDOException $e) {
			return false;
		}

		return true;
	}

	/**
	 * Create the Product table.
	 */
	private function create_product() {
		session_start();
		$this->set_migration_progress($_SESSION['processed'] + 1, 'Creating the Product table');

		try {
			$pdo = new PDO('mysql:host=' . $this->config['db']['host'] . ';dbname=' . $this->config['db']['dbname'], $this->config['db']['user'], $this->config['db']['pass']);

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

			# Create the table
			$pdo->exec("CREATE table `products` (
					`id` INT(11) AUTO_INCREMENT PRIMARY KEY,
					`label` VARCHAR(128) NOT NULL,
					`category_id` INT(11) NOT NULL,
					`type` VARCHAR(128) NOT NULL,
					`download_url` TEXT NULL,
					`weight` VARCHAR(128) NULL
				);");

			# Add sample data
			$sql = 'INSERT INTO products(label, category_id, type, download_url, weight) VALUES(:label, :category_id, :type, :download_url, :weight)';
			$stmt = $pdo->prepare($sql);

			for($i = 1; $i <= 5000; $i++) {
				$this->set_migration_progress($_SESSION['processed'] + 1, 'Adding data for Product ' . str_pad($i, 4, '0', STR_PAD_LEFT));

				$type = rand(0, 1) == 0 ? 'physical' : 'digital';
				$download_url = $type == 'digital' ? $this->util->base_url() . 'download/' . $this->util->random_str() : null;
				$weight = $type == 'physical' ? rand(0, 10) / 10 . 'lbs' : null;

				$stmt->execute([
					':label'        => 'Product ' . str_pad($i, 4, '0', STR_PAD_LEFT),
					':category_id'  => rand(1, 500),
					':type'         => $type,
					':download_url' => $download_url,
					':weight'       => $weight
				]);
			}
		} catch(PDOException $e) {
			return false;
		}

		return true;
	}

	/**
	 * Get all the products in the system.
	 */
	public function products() {
		try {
			$pdo = new PDO('mysql:host=' . $this->config['db']['host'] . ';dbname=' . $this->config['db']['dbname'], $this->config['db']['user'], $this->config['db']['pass']);

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

			$sql = "SELECT
					c.id AS category_id,
					c.label AS category_name,
					p.id AS product_id,
					p.label AS product_name,
					p.type,
					p.download_url,
					p.weight
				FROM categories c
					LEFT JOIN products p ON p.category_id = c.id
				ORDER BY
					c.label ASC,
					p.label ASC";

			$result = $pdo->query($sql)->fetchAll();

			# Organize the data
			$data = array();

			foreach($result as $row) {
				if(empty($data[$row['category_name']])) {
					$data[$row['category_name']] = array(
						'id'       => $row['category_id'],
						'name'     => $row['category_name'],
						'products' => array()
					);
				}

				$data[$row['category_name']]['products'][] = array(
					'id'           => $row['product_id'],
					'name'         => $row['product_name'],
					'type'         => $row['type'],
					'download_url' => $row['download_url'],
					'weight'       => $row['weight']
				);
			}

			return $data;
		} catch(PDOException $e) {
			return false;
		}
	}

	/**
	 * Get the product information.
	 */
	public function product($id) {
		try {
			$pdo = new PDO('mysql:host=' . $this->config['db']['host'] . ';dbname=' . $this->config['db']['dbname'], $this->config['db']['user'], $this->config['db']['pass']);

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

			$sql = "SELECT
					c.id AS category_id,
					c.label AS category_name,
					p.id AS product_id,
					p.label AS product_name,
					p.type,
					p.download_url,
					p.weight
				FROM categories c
					LEFT JOIN products p ON p.category_id = c.id
				WHERE p.id = :product_id
				ORDER BY
					c.label ASC,
					p.label ASC";

			$stmt = $pdo->prepare($sql);
			$stmt->execute(['product_id' => $id]);
			$result = $stmt->fetchAll();

			if(count($result) > 0) {
				foreach($result as $row) {
					return $row;
				}
			} else {
				return false;
			}
		} catch(PDOException $e) {
			return false;
		}
	}

	/**
	 * Create the order with the items provided.
	 */
	public function place_order($items) {
		try {
			$pdo = new PDO('mysql:host=' . $this->config['db']['host'] . ';dbname=' . $this->config['db']['dbname'], $this->config['db']['user'], $this->config['db']['pass']);

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

			# Add the order and get the order ID
			$sql = 'INSERT INTO orders(order_date) VALUES(SYSDATE())';
			$stmt = $pdo->prepare($sql);

			$stmt->execute();

			# Make sure to track the order ID
			$order_id = $pdo->lastInsertId();

			# Add the order items
			$sql = 'INSERT INTO order_items(order_id, product_id, quantity) VALUES(:order_id, :product_id, :quantity)';
			$stmt = $pdo->prepare($sql);

			foreach($items as $item) {
				$stmt->execute([
					':order_id'   => $order_id,
					':product_id' => intval($item['id']),
					':quantity'   => intval($item['quantity'])
				]);
			}

			return $order_id;
		} catch(PDOException $e) {
			return false;
		}
	}

	/**
	 * Get all available orders.
	 */
	public function orders() {
		try {
			$pdo = new PDO('mysql:host=' . $this->config['db']['host'] . ';dbname=' . $this->config['db']['dbname'], $this->config['db']['user'], $this->config['db']['pass']);

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

			$sql = "SELECT
					o.id AS order_id,
					DATE_FORMAT(o.order_date, '%M %d, %Y %h:%i:%s %p') AS order_date
				FROM orders o
				ORDER BY
					o.id ASC";

			$result = $pdo->query($sql)->fetchAll();

			return $result;
		} catch(PDOException $e) {
			return false;
		}
	}

	/**
	 * Get the order details.
	 */
	public function order_details($order_id) {
		try {
			$pdo = new PDO('mysql:host=' . $this->config['db']['host'] . ';dbname=' . $this->config['db']['dbname'], $this->config['db']['user'], $this->config['db']['pass']);

			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

			$sql = "SELECT
					o.id AS order_id,
					DATE_FORMAT(o.order_date, '%M %d, %Y %h:%i:%s %p') AS order_date,
					p.id AS product_id,
					p.label AS product_name,
					p.type,
					p.download_url,
					p.weight,
					oi.quantity
				FROM orders o
					LEFT JOIN order_items oi ON oi.order_id = o.id
					LEFT JOIN products p ON p.id = oi.product_id
				WHERE o.id = :order_id
				ORDER BY
					p.label ASC";

			$stmt = $pdo->prepare($sql);
			$stmt->execute(['order_id' => $order_id]);
			$result = $stmt->fetchAll();

			# Organize the data
			$data = array();

			foreach($result as $row) {
				if(empty($data[$row['order_id']])) {
					$data[$row['order_id']] = array(
						'id'    => $row['order_id'],
						'name'  => "Order-{$row['order_id']}",
						'items' => array()
					);
				}

				$data[$row['order_id']]['items'][] = array(
					'id'           => $row['product_id'],
					'name'         => $row['product_name'],
					'type'         => $row['type'],
					'download_url' => $row['download_url'],
					'weight'       => $row['weight'],
					'quantity'     => $row['quantity']
				);
			}

			return $data;
		} catch(PDOException $e) {
			return false;
		}
	}
}
?>