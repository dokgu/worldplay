<?php
namespace App\Helpers;

class Util {
	/**
	 * Gets a preformatted string of the variable dump.
	 * Uses output buffer to prevent errors.
	 *
	 * Very useful when the request was made with AJAX so you can dump the
	 * variable and return the contents to the script.
	 */
	public function dump_var($variable) {
		ob_start();
		echo "<pre style='text-align: left; white-space: pre-wrap;'>";
		var_dump($variable);
		echo "</pre>";
		$result = ob_get_contents();
		ob_end_clean();

		return $result;
	}

	/**
	 * Generate a random string, using a cryptographically secure
	 * pseudorandom number generator (random_int)
	 *
	 * This function uses type hints now (PHP 7+ only), but it was originally
	 * written for PHP 5 as well.
	 *
	 * For PHP 7, random_int is a PHP core function
	 * For PHP 5.x, depends on https://github.com/paragonie/random_compat
	 *
	 * @param int $length      How many characters do we want?
	 * @param string $keyspace A string of all possible characters
	 *                         to select from
	 * @return string
	 */
	public function random_str(int $length = 64, string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'): string {
		if($length < 1) {
			$length = 64;
		}

		$pieces = [];
		$max = mb_strlen($keyspace, '8bit') - 1;

		for($i = 0; $i < $length; ++$i) {
			$pieces[] = $keyspace[random_int(0, $max)];
		}

		return implode('', $pieces);
	}

	/**
	 * Get the base URL for the application.
	 */
	public function base_url() {
		# Get the base path.
		$server_name = $_SERVER['SERVER_NAME'];
		$server_port = $_SERVER['SERVER_PORT'];
		$dir = dirname(dirname(dirname(__FILE__)));
		$dir = str_replace('\\', '/', $dir); # Only for Windows servers
		$dir = str_replace($_SERVER['DOCUMENT_ROOT'], '', $dir);
		$path = $server_port == "80" ? "//{$server_name}{$dir}/" : "//{$server_name}:{$server_port}{$dir}/";

		return $path;
	}
}
?>