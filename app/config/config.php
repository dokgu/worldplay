<?php
namespace App\Config;

class Config {
	/**
	 * Return the default Slim configuration.
	 */
	public static function slim_config() {
		$config['displayErrorDetails'] = true;
		$config['addContentLengthHeader'] = false;

		$config['db']['host']   = DB_HOST;
		$config['db']['user']   = DB_USER;
		$config['db']['pass']   = DB_PASSWORD;
		$config['db']['dbname'] = DB_DATABASE;

		return $config;
	}
}
?>