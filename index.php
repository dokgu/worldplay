<?php
require_once 'vendor/autoload.php';
require_once 'const.php';

use \Slim\App;
use \Slim\Http\Stream;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \App\Config\Config;
use \App\Helpers\Util;
use \App\Database\Database;

$config = Config::slim_config();
$util = new Util();
$db = new Database();

# Create the Slim application
$app = new App(['settings' => $config]);

# Get the application container to add dependencies to
$container = $app->getContainer();

# Add a database connection (PDO)
$container['db'] = function($c) use ($util) {
	$db = $c['settings']['db'];

	try {
		$pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'], $db['user'], $db['pass']);

		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

		return $pdo;
	} catch(PDOException $e) {
		return array(
			'code'    => $e->getCode(),
			'message' => $e->getMessage()
		);
	}
};

# Add the ability to use views
$container['view'] = new \Slim\Views\PhpRenderer('views/');

# Create the frontend routes
$app->get('/', function(Request $request, Response $response, array $args) use ($util, $db) {
	$products = null;

	if($this->db instanceof PDO) {
		# Get the products to display
		$products = $db->products();
	} else {
		# Clear the session
		session_start();
		session_unset();
		session_destroy();
		session_write_close();
	}

	session_start();

	$response = $this->view->render($response, 'home.php', [
		'initialized' => $this->db instanceof PDO,
		'error_code'  => !($this->db instanceof PDO) ? $this->db['code'] : null,
		'products'    => $products,
		'base_url'    => $util->base_url(),
		'cart'        => !empty($_SESSION['cart']) ? $_SESSION['cart'] : null
	]);

	return $response;
})->setName('home');

$app->get('/orders', function(Request $request, Response $response, array $args) use ($util, $db) {
	$orders = null;

	if($this->db instanceof PDO) {
		# Get the orders to display
		$orders = $db->orders();
	} else {
		# Clear the session
		session_start();
		session_unset();
		session_destroy();
		session_write_close();
	}

	session_start();

	$response = $this->view->render($response, 'orders.php', [
		'initialized' => $this->db instanceof PDO,
		'error_code'  => !($this->db instanceof PDO) ? $this->db['code'] : null,
		'base_url'    => $util->base_url(),
		'orders'      => $orders,
		'cart'        => !empty($_SESSION['cart']) ? $_SESSION['cart'] : null
	]);

	return $response;
})->setName('orders');

# Create the API endpoints
$app->get('/migrate-db', function(Request $request, Response $response, array $args) use ($util, $db) {
	$db->init();
});

$app->get('/migration-progress', function(Request $request, Response $response, array $args) use ($util, $db) {
	$progress = $db->migration_progress();

	$response = $response->withJson($progress);
	return $response;
});

$app->post('/api/cart/products/{product-id}', function(Request $request, Response $response, array $args) use ($util, $db) {
	$product_id = intval($args['product-id']);
	$product = $db->product($product_id);

	# Initialize the session
	session_start();

	if(empty($_SESSION['cart'])) {
		# Empty cart
		$_SESSION['cart'] = array($product_id => array(
			'product' => $product,
			'quantity' => 1
		));
	} else if(empty($_SESSION['cart'][$product_id])) {
		# Product not in cart yet
		$_SESSION['cart'][$product_id] = array(
			'product' => $product,
			'quantity' => 1
		);
	} else {
		# Product already in cart - increment the count
		$_SESSION['cart'][$product_id]['quantity'] = $_SESSION['cart'][$product_id]['quantity'] + 1;
	}

	# Write the data
	session_write_close();

	$response = $response->withJson($_SESSION['cart']);
	return $response;
});

$app->get('/api/cart', function(Request $request, Response $response, array $args) use ($util, $db) {
	# Initialize the session
	session_start();

	if(empty($_SESSION['cart'])) {
		$_SESSION['cart'] = array();
	}

	# Write the data
	session_write_close();

	$response = $response->withJson($_SESSION['cart']);
	return $response;
});

$app->post('/api/order', function(Request $request, Response $response, array $args) use ($util, $db) {
	$data = $request->getParsedBody();
	$items = $data['items'];

	# Place the order
	$order_id = $db->place_order($items);

	# Clear the cart if successful, otherwise leave the cart so the user can attempt to place the order again
	if(!empty($order_id)) {
		session_start();
		unset($_SESSION['cart']);
	}

	$response = $response->withJson(array(
		'order_id'    => $order_id,
		'orders_page' => $util->base_url() . 'orders'
	));
	return $response;
});

$app->get('/api/order/{order-id}', function(Request $request, Response $response, array $args) use ($util, $db) {
	$order_id = intval($args['order-id']);
	$order_details = $db->order_details($order_id);

	$response = $response->withJson($order_details);
	return $response;
});

$app->get('/download/{download-id}', function(Request $request, Response $response, array $args) use ($util, $db) {
	$file = __DIR__ . '/downloads/download.txt';
	$handler = fopen($file, 'rb');

	$stream = new Stream($handler);

	return $response->withHeader('Content-Type', 'application/force-download')
		->withHeader('Content-Type', 'application/octet-stream')
		->withHeader('Content-Type', 'application/download')
		->withHeader('Content-Description', 'File Transfer')
		->withHeader('Content-Transfer-Encoding', 'binary')
		->withHeader('Content-Disposition', 'attachment; filename="' . basename($file) . '"')
		->withHeader('Expires', '0')
		->withHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
		->withHeader('Pragma', 'public')
		->withBody($stream);
});

# Run the Slim application
$app->run();
?>