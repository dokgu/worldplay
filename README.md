## Requirements

[![php](https://img.shields.io/badge/PHP-7.4-blue.svg)](https://gitlab.com/dokgu/worldplay) [![mysql](https://img.shields.io/badge/MySQL-10.4-blue)](https://gitlab.com/dokgu/worldplay)

This repository requires the use of [Composer](https://getcomposer.org/).

## Cloning Instructions

1. `git clone git@gitlab.com:dokgu/worldplay.git` - clone the repository
2. `cd worldplay` - go to the project folder
3. `cp const.example.php const.php` - copy the constants file and enter the database credentials in `const.php`
4. `composer install` - install all project dependencies

## Sample CURL Command

An example of a CURL command to place an order for `Product 0003` _(quantity of `1`)_:

```bash
curl 'http://localhost/worldplay/api/order' \
  -H 'Accept: */*' \
  -H 'Accept-Language: en' \
  -H 'Connection: keep-alive' \
  -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' \
  -H 'Cookie: PHPSESSID=8h5utl15lk422ooancefcr9i8i' \
  -H 'Origin: http://localhost' \
  -H 'Referer: http://localhost/worldplay/' \
  -H 'Sec-Fetch-Dest: empty' \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' \
  -H 'X-Requested-With: XMLHttpRequest' \
  -H 'sec-ch-ua: "Chromium";v="104", " Not A;Brand";v="99", "Google Chrome";v="104"' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'sec-ch-ua-platform: "Windows"' \
  --data-raw 'items%5B0%5D%5Bid%5D=3&items%5B0%5D%5Bquantity%5D=1' \
  --compressed
```