<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Online Store - Worldplay</title>
		<meta name="description" content="">
		<meta charset="utf-8">
		<meta name="description" content="Online Store - Worldplay" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<!-- Meta data for when sharing link -->
		<meta property="og:title" content="Online Store - Worldplay" />
		<meta property="og:description" content="Online Store - Worldplay" />
		<meta property="og:type" content="website" />
		<meta property="og:locale" content="en_CA" />
		<meta property="og:locale:alternate" content="en_US" />
		<link rel="icon" href="<?php echo $base_url; ?>images/favicon.ico" />
		<!-- Google Font: Lato -->
		<link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<!-- Bootstrap -->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
		<link rel="stylesheet" href="<?php echo $base_url; ?>css/bootstrap/bootstrap-docs.css?v=1.0.0">
		<!-- FontAwesome -->
		<link rel="stylesheet" href="<?php echo $base_url; ?>css/font-awesome/css/all.min.css?v=1.0.0">
		<!-- Custom CSS -->
		<link rel="stylesheet" href="<?php echo $base_url; ?>css/custom.css?v=1.0.2">
		<!-- jQuery -->
		<script src="<?php echo $base_url; ?>js/jquery.min.js?v=1.0.0"></script>
		<!-- Custom Script -->
		<script src="<?php echo $base_url; ?>js/custom.js?v=1.0.1"></script>
	</head>
	<body>
		<div class="container <?php if(!$initialized) { echo 'vertical-middle'; } ?>">
			<?php
			if($initialized) {
				# Database is up and running
				echo "<ul class='nav justify-content-end'>";

				echo "<li class='nav-item'>";
				echo "<a class='nav-link active' aria-current='page' href='{$base_url}'>Shop</a>";
				echo "</li>";

				echo "<li class='nav-item'>";
				echo "<a class='nav-link' href='{$base_url}orders'>Orders</a>";
				echo "</li>";

				echo "<li class='nav-item'>";
				echo "<a class='nav-link shopping-cart' href='#' data-bs-toggle='offcanvas' data-bs-target='#staticBackdrop' aria-controls='staticBackdrop' data-base-url='{$base_url}'>";
				echo "<i class='fas fa-shopping-bag'></i>";
				$count = 0;
				if(!empty($cart) && count($cart) > 0) {
					foreach($cart as $item) {
						$count += $item['quantity'];
					}
				}
				echo "<span id='cart-count' class='cart-count'>{$count}</span>";
				echo "</a>";
				echo "</li>";

				echo "</ul>";

				echo "<div class='offcanvas offcanvas-end' data-bs-backdrop='static' tabindex='-1' id='staticBackdrop' aria-labelledby='staticBackdropLabel'>";
				echo "<div class='offcanvas-header'>";
				echo "<h5 class='offcanvas-title' id='staticBackdropLabel'>Cart</h5>";
				echo "<button type='button' class='btn-close' data-bs-dismiss='offcanvas' aria-label='Close'></button>";
				echo "</div>";
				echo "<div id='cart-offcanvas-body' class='offcanvas-body'></div>";
				echo "</div>";

				echo "<div class='row'>";

				echo "<div class='col'>";
				echo "<div class='list-group'>";

				foreach($products as $category) {
					echo "<a href='#' id='category-{$category['id']}' class='list-group-item list-group-item-action category' data-id='{$category['id']}'>";
					echo "<span class='category-label'>{$category['name']}</span>";
					echo "<span class='badge bg-primary'>" . count($category['products']) . "</span>";
					echo "</a>";
				}

				echo "</div>";
				echo "</div>";

				echo "<div class='col-10'>";

				$display = null;
				foreach($products as $category) {
					echo "<div id='category-items-{$category['id']}' class='row {$display} category-items-container'>";

					echo "<div class='grid'>";
					echo "<div class='g-col-12'>";
					echo "<h2>{$category['name']}</h2>";
					echo "</div>";
					echo "</div>";

					echo "<div class='grid'>";
					foreach($category['products'] as $product) {
						echo "<div class='g-col-4'>";

						echo "<div class='card'>";
						echo "<img src='images/350x150.png' class='card-img-top' alt='{$product['name']}'>";
						if($product['type'] == 'digital') {
							echo "<span class='badge bg-danger'>Downloadable</span>";
						}
						echo "<div class='card-body'>";
						echo "<h5 class='card-title'>{$product['name']}</h5>";
						echo "<p class='card-text small'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non ligula a augue pellentesque pharetra vel in nunc.</p>";
						if($product['type'] == 'digital') {
							echo "<p class='card-text small'><span class='bold label'>Download:</span><span>after purchase</span></p>";
						} else {
							echo "<p class='card-text small'><span class='bold label'>Weight:</span><span>{$product['weight']}</span></p>";
						}
						echo "<div class='text-center'>";
						echo "<a href='#' class='btn btn-primary add-to-cart' data-id='{$product['id']}' data-base-url='{$base_url}'>Add to Cart</a>";
						echo "</div>";
						echo "</div>";
						echo "</div>";

						echo "</div>";
					}
					echo "</div>";

					echo "</div>";

					if(empty($display)) {
						$display = 'display-none';
					}
				}

				echo "</div>";

				echo "</div>";
			} else {
				# Error connecting to the database
				echo "<div class='text-center'>";
				echo "<h2>Database</h2>";
				switch ($error_code) {
					case 1049:
						# Unknown database
						echo "<p class='context'>Application hasn't been initialized. The database needs to be migrated first.</p>";
						echo "<button type='button' id='migrate-db' class='btn btn-primary' data-base-url='{$base_url}'>Migrate Database</button>";
						break;
					
					default:
						# Generic error
						echo "<p class='context'>Unable to connect to the database. Please make sure the database is running and the proper credentials are stored in the <code>const.php</code> file from the root directory. Once that's done, simply reload the page to try again.</p>";
						echo "<button type='button' id='reload-page' class='btn btn-primary' data-base-url='{$base_url}'>Reload Page</button>";
						break;
				}
				echo "</div>";
			}
			?>
		</div>
		<!-- Bootstrap -->
		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
	</body>
</html>