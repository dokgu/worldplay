$(document).ready(function() {
	$(document).on('click', '#reload-page', function(e) {
		e.preventDefault();

		window.location.href = $(this).data('base-url');
	});

	$(document).on('click', '#migrate-db', function(e) {
		e.preventDefault();

		var btn = $(this);

		$.ajax({
			url: btn.data('base-url') + 'migrate-db',
			method: 'GET',
			beforeSend: function(jqXHR, settings) {
				btn.replaceWith('<div class="progress"><div class="progress-bar" role="progressbar" style="width: 0%" aria-label="Database migration progress" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div></div>');
			},
			success: function(data, textStatus, jqXHR) {
				//console.log('migrate-db success');
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
			}
		});

		setInterval(function() {
			$.ajax({
				url: btn.data('base-url') + 'migration-progress',
				method: 'GET',
				beforeSend: function(jqXHR, settings) {
					//
				},
				success: function(data, textStatus, jqXHR) {
					$('.context').text(data.description);
					$('.progress-bar').css('width', data.progress + '%').attr('aria-valuenow', data.progress);

					if(data.progress >= 100) {
						setTimeout(function() {
							window.location.href = btn.data('base-url');
						}, 1500);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
				}
			});
		}, 300);
	});

	$(document).on('click', '.category', function(e) {
		e.preventDefault();

		// Hide all
		$('.category-items-container').addClass('display-none');

		// Display the selected category
		$('#category-items-' + $(this).data('id')).removeClass('display-none');

		// Scroll to the top of the page
		$('html, body').animate({ scrollTop: 0 }, 100);
	});

	$(document).on('click', '.add-to-cart', function(e) {
		e.preventDefault();

		var link = $(this);
		var product_id = link.data('id');

		$.ajax({
			url: link.data('base-url') + 'api/cart/products/' + product_id,
			method: 'POST',
			beforeSend: function(jqXHR, settings) {
				//
			},
			success: function(data, textStatus, jqXHR) {
				var quantity = 0;
				$.each(data, function(index, item) {
					quantity += parseInt(item.quantity);
				});
				$('#cart-count').text(quantity);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
			}
		});
	});

	$(document).on('click', '.shopping-cart', function(e) {
		e.preventDefault();

		var link = $(this);

		$.ajax({
			url: link.data('base-url') + 'api/cart',
			method: 'GET',
			beforeSend: function(jqXHR, settings) {
				$('#cart-offcanvas-body').empty();
			},
			success: function(data, textStatus, jqXHR) {
				var html = '<div>';

				if((!!data) && (data.constructor === Object)) {
					$.each(data, function(index, item) {
						html += '<div class="item-container mb-3" data-id="' + item.product.product_id + '" data-quantity="' + item.quantity + '">';
						html += '<img src="images/80x80.png" alt="' + item.product.product_name + '" />';
						html += '<span class="item-name">' + item.product.product_name + '</span>';
						html += '<span class="item-quantity">' + item.quantity + '</span>';
						html += '</div>';
					});

					html += '<div class="d-grid gap-2">';
					html += '<button id="btn-order" class="btn btn-primary" type="button" data-base-url="' + link.data('base-url') + '">Place Order</button>';
					html += '</div>';
				} else {
					html += '<p>There are no items in your cart, go to the <a href="' + link.data('base-url') + '">Shop</a> page and add some - all items are <span class="bold">FREE!</span></p>';
				}

				html += '</div>';

				$('#cart-offcanvas-body').append(html);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
			}
		});
	});

	$(document).on('click', '#btn-order', function(e) {
		e.preventDefault();

		var btn = $(this);

		var items = $('#cart-offcanvas-body .item-container');
		var data = [];

		items.each(function(index) {
			data.push({
				id: $(this).data('id'),
				quantity: $(this).data('quantity')
			});
		});

		$.ajax({
			url: btn.data('base-url') + 'api/order',
			method: 'POST',
			data: {
				items: data
			},
			beforeSend: function(jqXHR, settings) {
				//
			},
			success: function(data, textStatus, jqXHR) {
				window.location.href = data.orders_page;
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
			}
		});
	});

	$(document).on('click', '.btn-order-details', function(e) {
		e.preventDefault();

		var btn = $(this);

		$.ajax({
			url: btn.data('base-url') + 'api/order/' + btn.data('id'),
			method: 'GET',
			beforeSend: function(jqXHR, settings) {
				$('#modal-title').text('');
				$('#modal-body').empty();
			},
			success: function(data, textStatus, jqXHR) {
				if((!!data) && (data.constructor === Object)) {
					var html = '<div>';

					$.each(data, function(index, order) {
						$('#modal-title').text(order.name);

						if(order.items.length) {
							$.each(order.items, function(index, item) {
								html += '<div class="item-container mb-3" data-id="' + item.id + '" data-quantity="' + item.quantity + '">';
								html += '<img src="images/80x80.png" alt="' + item.name + '" />';
								if(item.type == 'digital') {
									html += '<span class="item-name"><a href="' + item.download_url + '">' + item.name + '</a></span>';
								} else {
									html += '<span class="item-name">' + item.name + '</span>';
								}
								html += '<span class="item-quantity">' + item.quantity + '</span>';
								html += '</div>';
							});
						}
					});

					html += '</div>';

					$('#modal-body').append(html);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
			}
		});
	});
});